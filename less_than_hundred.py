#!/usr/bin/env python
# encoding: utf-8
import re
import os
import imp
import signal
import webbrowser

from tornado import gen
from tornado.ioloop import IOLoop
from tornado.options import define, options, parse_command_line, \
    parse_config_file
from tornado.web import Application, RequestHandler
from tornado.httpclient import AsyncHTTPClient
import bs4

NAME = 'xa6pokcug'
WORD_RE = re.compile(
    r'(?:^|(?<=\s))(\w{6})(?:(?=\s)|$)', flags=(re.MULTILINE | re.UNICODE)
)
IGNORE_TAGS = ('script', 'style', 'pre', 'code', 'iframe')


define('scheme', default='http', help="")
define('address', default='127.0.0.1', help="address to listen on")
define('port', default=8888, help="port to listen on")
define('config_file', default='config.cfg',
       help='filename for additional configuration')

define('debug', default=False, group='application',
       help="run in debug mode (with automatic reloading)")
define('proxy_host', default='habrahabr.ru', group='application',
       help="host proxy to")


def on_start():
    url = "{scheme}://{address}:{port}".format(
        scheme=options.scheme, address=options.address, port=options.port
    )

    webbrowser.open(url, new=2)


class MainHandler(RequestHandler):
    @gen.coroutine
    def get(self):
        url = '{protocol}://{host}{path}'.format(
            protocol=self.request.protocol, host=options.proxy_host,
            path=self.request.uri
        )

        try:
            response = yield AsyncHTTPClient().fetch(url)
            content = response.body
        except Exception as error:
            response = error.response
            content = response and response.body

        content = self.process(content)
        self.set_status(response.code)
        self.write(content)

    def process(self, content):
        soup = bs4.BeautifulSoup(content, 'html5lib')

        domain_re = self.application.settings.get('domain_re')
        for tag in domain_re and soup.find_all('a', href=domain_re) or ():
            tag.attrs['href'] = domain_re.sub(u'', tag.attrs['href'])

        for node in soup.find_all(text=True):
            if node.string.strip() \
                and not isinstance(node, bs4.element.PreformattedString) \
                and node.parent.name not in IGNORE_TAGS:
                node.string.replace_with(WORD_RE.sub(u'\\1™', node.string))

        return u'{0}'.format(soup))


def main():
    parse_command_line(final=False)
    if os.path.isfile(options.config_file):
        parse_config_file(options.config_file)

    domain_re = re.compile(r'^https?://{0}'.format(options.proxy_host))

    app = Application([
        (r".+", MainHandler),
    ], domain_re=domain_re, **options.group_dict('application'))

    app.listen(options.port, options.address)
    ioloop = IOLoop.current()
    if not options.debug:
        ioloop.add_callback(on_start)

    ioloop.start()


if __name__ == "__main__":
    main()
